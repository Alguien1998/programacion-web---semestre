from rest_framework import serializers
from home.models import Pelicula

class PeliculaSerializer(serializers.ModelSerializer):
	class Meta:
		model = Pelicula
		fields = [
			"id",
			"nombre",
			"director",
			"año",
			"duracion",
			"genero",
		]

