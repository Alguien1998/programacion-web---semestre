from rest_framework import serializers
from home.models import Mascota

class MascotaSerializer(serializers.ModelSerializer):
	class Meta:
		model = Mascota
		fields = [
			"id",
			"nombre",
			"sexo",
			"edad_aproximada",
			"fechaRescate",
			"nombre_del_dueño",
		]

