# Generated by Django 2.2.4 on 2019-09-18 21:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='credencial',
            name='no_de_Control',
            field=models.IntegerField(),
        ),
        migrations.AlterField(
            model_name='credencial',
            name='telefono',
            field=models.IntegerField(),
        ),
    ]
