from rest_framework import serializers
from home.models import Credencial

class CredencialSerializer(serializers.ModelSerializer):
	class Meta:
		model = Credencial
		fields = [
			"id",
			"nombre",
			"apePat",
			"apeMat",
			"no_de_Control",
			"carrera",
			"curp",
			"correo_institucional",
			"tipo_de_sangre",
			"telefono",
		]

