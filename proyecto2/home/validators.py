from django.core.exceptions import ValidationError

def validation_edad_aproximada(value):
	edad_aproximada = value
	if (edad_aproximada > 15):
		raise ValidationError("The max edad_aproximada is 15")
	return value