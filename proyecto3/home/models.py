from django.db import models
from django.db.models.signals import post_save, pre_save, post_delete, pre_delete
from home import validators, signals
# Create your models here.
# Create your models here.
class Registro(models.Model):
	nombre = models.CharField(max_length=32)
	apePat = models.CharField(max_length=32)
	apeMat = models.CharField(max_length=32)
	nacionalidad = models.CharField(max_length=32, validators = [validators.validation_nacionalidad])
	fechaNac = models.DateField(default = '1998-01-01')
	lugarNac = models.CharField(max_length=32)
	correo = models.EmailField()


	def __str__(self):
		return self.nombre

post_save.connect(signals.post_save_home_receiver, sender = Registro)
pre_save.connect(signals.pre_save_home_receiver, sender = Registro)
post_delete.connect(signals.post_delete_home_receiver, sender = Registro)
pre_delete.connect(signals.pre_delete_home_receiver, sender = Registro)