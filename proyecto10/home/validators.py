from django.core.exceptions import ValidationError

def validation_memoria(value):
	memoria = value
	if (memoria > 512):
		raise ValidationError("The max memoria is 512")
	return value