from django.db import models
from django.db.models.signals import post_save, pre_save, post_delete, pre_delete
from home import validators, signals
# Create your models here.
class Tarjeta(models.Model):
	nombre_en_la_tarjeta = models.CharField(max_length=32)
	tipo_de_tarjeta = models.CharField(max_length=32)
	fecha_de_vencimiento = models.DateField()
	CVV2_CVC2 = models.IntegerField()
	NIP = models.IntegerField(validators = [validators.validation_NIP])

	def __str__(self):
		return self.nombre_en_la_tarjeta

post_save.connect(signals.post_save_home_receiver, sender = Tarjeta)
pre_save.connect(signals.pre_save_home_receiver, sender = Tarjeta)
post_delete.connect(signals.post_delete_home_receiver, sender = Tarjeta)
pre_delete.connect(signals.pre_delete_home_receiver, sender = Tarjeta)