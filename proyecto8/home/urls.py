from django.urls import path

from home import views

urlpatterns = [
    path('list/', views.list, name = "list"),
    path('detail/<int:id>', views.detail, name = "detail"),
    path('create/', views.create, name = "create"),
    path('update/<int:id>/', views.update, name="update"),
    path('delete/<int:id>/', views.delete, name="delete"),

    path('endpoint/', views.VideojuegoListAPIView.as_view(), name="VideojuegoSerializer"),
    path('endpoint/create', views.VideojuegoCreateAPIView.as_view(), name="VideojuegoCreateSerializer"),

    path('list2/', views.List.as_view(), name="list2"),
    path('detail2/<int:pk>/', views.Detail.as_view(), name="detail2"),
    path('create2/', views.Create.as_view(), name="create2"),
    path('delete2/<int:pk>/', views.Delete.as_view(), name="delete2"),
    path('update2/<int:pk>/', views.Update.as_view(), name="update2"),
    

    path('1', views.index1, name = "index1_view"),
    path('2', views.index2, name = "index2_view"),
    path('3', views.index3, name = "index3_view"),
    path('4', views.index4, name = "index4_view"),
    path('5', views.index5, name = "index5_view"),
    path('6', views.index6, name = "index6_view"),
    path('7', views.index7, name = "index7_view"),
    path('8', views.index8, name = "index8_view"),
    path('9', views.index9, name = "index9_view"),
    path('10', views.index10, name = "index10_view"),
]