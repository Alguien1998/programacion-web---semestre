from django.db import models
from django.db.models.signals import post_save, pre_save, post_delete, pre_delete
from home import validators, signals

# Create your models here.
class Videojuego(models.Model):
	nombre = models.CharField(max_length=32)
	plataforma = models.CharField('Plataforma(s)',max_length=60)
	fecha_de_lanzamiento = models.CharField(max_length=32)
	genero = models.CharField(max_length=32)
	formato = models.CharField(max_length=32, validators = [validators.validation_formato])

	def __str__(self):
		return self.nombre

post_save.connect(signals.post_save_home_receiver, sender = Videojuego)
pre_save.connect(signals.pre_save_home_receiver, sender = Videojuego)
post_delete.connect(signals.post_delete_home_receiver, sender = Videojuego)
pre_delete.connect(signals.pre_delete_home_receiver, sender = Videojuego)