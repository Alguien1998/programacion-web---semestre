from django import forms

from .models import Mascota

class MascotaForm(forms.ModelForm):
	class Meta:
		model = Mascota
		fields = [
		"nombre",
		"sexo",
		"edad_aproximada",
		"fechaRescate",
		"nombre_del_dueño",
		]