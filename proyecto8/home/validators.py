from django.core.exceptions import ValidationError

def validation_formato(value):
	formato = value
	if (formato == "VHS"):
		raise ValidationError("Formato VHS no permitido")
	return value