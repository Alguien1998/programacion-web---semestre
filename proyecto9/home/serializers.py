from rest_framework import serializers
from home.models import Inventario

class InventarioSerializer(serializers.ModelSerializer):
	class Meta:
		model = Inventario
		fields = [
			"id",
			"nombre",
			"codigo",
			"descripcion",
			"precio",
			"tamano",
		]

