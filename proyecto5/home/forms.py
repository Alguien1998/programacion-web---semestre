from django import forms


from .models import Tarjeta


class TarjetaForm(forms.ModelForm):
	class Meta:
		model = Tarjeta
		fields = [
		"nombre_en_la_tarjeta",
		"tipo_de_tarjeta",
		"fecha_de_vencimiento",
		"CVV2_CVC2",
		"NIP",
		]