from rest_framework import serializers
from home.models import Tarjeta

class TarjetaSerializer(serializers.ModelSerializer):
	class Meta:
		model = Tarjeta
		fields = [
			"id",
			"nombre_en_la_tarjeta",
			"tipo_de_tarjeta",
			"fecha_de_vencimiento",
			"CVV2_CVC2",
			"NIP",
		]

