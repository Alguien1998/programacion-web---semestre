from django.db import models
from django.db.models.signals import post_save, pre_save, post_delete, pre_delete
from home import validators, signals

# Create your models here.
class Inventario(models.Model):
	nombre = models.CharField(max_length=32)
	codigo = models.CharField(max_length=60)
	descripcion = models.CharField(max_length=50)
	precio = models.DecimalField('Precio ($)', max_digits=10, decimal_places=2, 
		validators = [validators.validation_precio])
	tamano = models.CharField('Cantidad (Tamaño o peso)',max_length=32)

	def __str__(self):
		return self.nombre

ost_save.connect(signals.post_save_home_receiver, sender = Inventario)
pre_save.connect(signals.pre_save_home_receiver, sender = Inventario)
post_delete.connect(signals.post_delete_home_receiver, sender = Inventario)
pre_delete.connect(signals.pre_delete_home_receiver, sender = Inventario)