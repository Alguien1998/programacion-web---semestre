from django.db import models
from django.db.models.signals import post_save, pre_save, post_delete, pre_delete
from home import validators, signals

# Create your models here.
class Pokemon(models.Model):
	nombre = models.CharField(max_length=32)
	tipo = models.CharField(max_length=32)
	nivel = models.IntegerField(validators = [validators.validation_nivel])


	def __str__(self):
		return self.nombre

post_save.connect(signals.post_save_home_receiver, sender = Pokemon)
pre_save.connect(signals.pre_save_home_receiver, sender = Pokemon)
post_delete.connect(signals.post_delete_home_receiver, sender = Pokemon)
pre_delete.connect(signals.pre_delete_home_receiver, sender = Pokemon)