from rest_framework import serializers
from home.models import Celular

class CelularSerializer(serializers.ModelSerializer):
	class Meta:
		model = Celular
		fields = [
			"id",
			"modelo",
			"marca",
			"memoria",
			"color",
			"caracteristica",
		]

