from django.db import models
from django.db.models.signals import post_save, pre_save, post_delete, pre_delete
from home import validators, signals


# Create your models here.
class Cancion(models.Model):
	nombre = models.CharField(max_length=32)
	album = models.CharField(max_length=32)
	intérprete = models.CharField(max_length=32)
	compositor = models.CharField(max_length=32)
	año = models.IntegerField(validators = [validators.validation_año])
	duracion = models.CharField('Duracion en mins', max_length=5)
	genero = models.CharField(max_length=32)

	def __str__(self):
		return self.nombre

post_save.connect(signals.post_save_home_receiver, sender = Cancion)
pre_save.connect(signals.pre_save_home_receiver, sender = Cancion)
post_delete.connect(signals.post_delete_home_receiver, sender = Cancion)
pre_delete.connect(signals.pre_delete_home_receiver, sender = Cancion)