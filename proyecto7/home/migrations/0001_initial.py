# Generated by Django 2.2.4 on 2019-09-19 19:31

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Cancion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=32)),
                ('album', models.CharField(max_length=32)),
                ('intérprete', models.CharField(max_length=32)),
                ('compositor', models.CharField(max_length=32)),
                ('año', models.IntegerField()),
                ('duracion', models.CharField(max_length=5, verbose_name='Duracion en mins')),
                ('genero', models.CharField(max_length=32)),
            ],
        ),
    ]
