from .models import Cancion
import time

def messageVersion(request):
	context = {"msgVersion":"Page v0.0.1",}
	return context


def count(request):
	context = {"count": Cancion.objects.count(),}
	return context

def getDate(self):
	context = {"date": time.strftime("%c")}
	return context