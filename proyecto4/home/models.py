from django.db import models
from django.db.models.signals import post_save, pre_save, post_delete, pre_delete
from home import validators, signals

# Create your models here.
class Credencial(models.Model):
	nombre = models.CharField(max_length=32)
	apePat = models.CharField(max_length=32)
	apeMat = models.CharField(max_length=32)
	no_de_Control = models.IntegerField()
	carrera = models.CharField(max_length=32)
	curp = models.CharField(max_length=32)
	correo_institucional = models.EmailField()
	tipo_de_sangre = models.CharField(max_length=3)
	telefono = models.IntegerField(validators = [validators.validation_telefono])


	def __str__(self):
		return self.nombre

post_save.connect(signals.post_save_home_receiver, sender = Credencial)
pre_save.connect(signals.pre_save_home_receiver, sender = Credencial)
post_delete.connect(signals.post_delete_home_receiver, sender = Credencial)
pre_delete.connect(signals.pre_delete_home_receiver, sender = Credencial)