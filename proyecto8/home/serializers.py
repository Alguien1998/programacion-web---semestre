from rest_framework import serializers
from home.models import Videojuego

class VideojuegoSerializer(serializers.ModelSerializer):
	class Meta:
		model = Videojuego
		fields = [
			"id",
			"nombre",
			"plataforma",
			"fecha_de_lanzamiento",
			"genero",
			"formato",
		]

