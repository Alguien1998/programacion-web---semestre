from django.core.exceptions import ValidationError

def validation_telefono(value):
	telefono = value
	if (telefono == 999999999):
		raise ValidationError("Numero no valido")
	return value