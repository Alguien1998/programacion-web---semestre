from django import forms


from .models import Credencial


class CredencialForm(forms.ModelForm):
	class Meta:
		model = Credencial
		fields = [
		"nombre",
		"apePat",
		"apeMat",
		"no_de_Control",
		"carrera",
		"curp",
		"correo_institucional",
		"tipo_de_sangre",
		"telefono",
		]