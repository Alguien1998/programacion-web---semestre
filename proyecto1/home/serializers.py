from rest_framework import serializers
from home.models import Pokemon

class PokemonSerializer(serializers.ModelSerializer):
	class Meta:
		model = Pokemon
		fields = [
			"id",
			"nombre",
			"tipo",
			"nivel",
		]

