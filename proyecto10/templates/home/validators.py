from django.core.exceptions import ValidationError

def validation_semester(value):
	nivel = value
	if (nivel > 101):
		raise ValidationError("The max nivel is 100")
	return value