from django.shortcuts import render, redirect
from django.views import generic
from django.urls import reverse_lazy
from django.db.models import Q

# Create your views here.
from .models import Inventario
from .forms import InventarioForm

from .serializers import InventarioSerializer
from rest_framework import generics
from rest_framework import viewsets

class InventarioListAPIView(generics.ListAPIView):
	serializer_class = InventarioSerializer

	def get_queryset(self, *args, **kwargs):
		return Inventario.objects.all()



class InventarioCreateAPIView(generics.CreateAPIView):
	serializer_class = InventarioSerializer

	def perform_create(self, serializer):
		serializer.save()


def list(request):
	inventarios = Inventario.objects.all()
	if request.GET:
		inventarios = Inventario.objects.filter(nombre = request.GET["q"])
	context = {
	"list_inventarios": inventarios
	}
	return render(request, "home/list.html", context)

def detail(request, id):
	queryset = Inventario.objects.get(id=id)
	context = {
	"object": queryset
	}
	return render(request, "home/detail.html", context)

def create(request):
	form = InventarioForm(request.POST or None)
	if request.user.is_authenticated:
		error = "User Logged"
		if form.is_valid():
			form.save()
	else:
		error = "User Must Be Logged"

	context = {
	"form": form,
	"menssage": error
	}
	return render(request, "home/create.html", context)

def update(request, id):
	inventario = Inventario.objects.get(id=id)
	if  request.method == "GET":
		form = InventarioForm(instance=inventario )
	else:
		form = InventarioForm(request.POST, instance=inventario)
		if form.is_valid():
			form.save()
		return redirect("list")
	context = {
	"form": form
	}
	return render(request, "home/update.html", context)


def delete(request, id):
	inventario = Inventario.objects.get(id=id)
	if request.method == "POST":
		inventario.delete()
		return redirect("list")
	context = {
		"object": inventario
	}
	return render(request, "home/delete.html", context)




class Create(generic.CreateView):
	template_name = "home/create2.html"
	model = Inventario
	fields = [
		"nombre",
		"codigo",
		"descripcion",
		"precio",
		"tamano",
	]
	success_url = reverse_lazy("list")

class List(generic.ListView):
	template_name = "home/list2.html"
	model = Inventario

	def get_queryset(self, *args, **kwargs):
		return Inventario.objects.all()

	def get_context_data(self, *args, **kwargs):
		context = super(List, self).get_context_data(*args, **kwargs)
		return context

class Detail(generic.DetailView):
	template_name = "home/detail2.html"
	model = Inventario

class Update(generic.UpdateView):
	template_name = "home/update2.html"
	model = Inventario
	fields = [
		"nombre",
		"codigo",
		"descripcion",
		"precio",
		"tamano",
	]
	success_url = reverse_lazy("list")

class Delete(generic.DeleteView):
	template_name = "home/delete2.html"
	model = Inventario
	success_url = reverse_lazy("list")


def index1(request):
	return render(request, "home/index.html", {})

def index2(request):
	return render(request, "home/index2.html", {})

def index3(request):
	return render(request, "home/index3.html", {})

def index4(request):
	return render(request, "home/index4.html", {})

def index5(request):
	return render(request, "home/index5.html", {})

def index6(request):
	return render(request, "home/index6.html", {})

def index7(request):
	return render(request, "home/index7.html", {})

def index8(request):
	return render(request, "home/index8.html", {})

def index9(request):
	return render(request, "home/index9.html", {})

def index10(request):
	return render(request, "home/index10.html", {})