from django.core.exceptions import ValidationError

def validation_precio(value):
	precio = value
	if (precio > 10000):
		raise ValidationError("The max precio is 10000")
	return value