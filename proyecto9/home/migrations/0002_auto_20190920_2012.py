# Generated by Django 2.2.4 on 2019-09-20 20:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='inventario',
            name='precio',
            field=models.DecimalField(decimal_places=2, max_digits=10, verbose_name='Precio ($)'),
        ),
    ]
