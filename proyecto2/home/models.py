from django.db import models
from django.db.models.signals import post_save, pre_save, post_delete, pre_delete
from home import validators, signals

# Create your models here.
class Mascota(models.Model):
	nombre = models.CharField(max_length=32)
	sexo = models.CharField(max_length=32)
	edad_aproximada = models.IntegerField(validators = [validators.validation_edad_aproximada])
	fechaRescate = models.DateField()
	nombre_del_dueño = models.CharField(max_length=32)


	def __str__(self):
		return self.nombre

post_save.connect(signals.post_save_home_receiver, sender = Mascota)
pre_save.connect(signals.pre_save_home_receiver, sender = Mascota)
post_delete.connect(signals.post_delete_home_receiver, sender = Mascota)
pre_delete.connect(signals.pre_delete_home_receiver, sender = Mascota)