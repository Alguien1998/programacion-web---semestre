from django import forms


from .models import Videojuego


class VideojuegoForm(forms.ModelForm):
	class Meta:
		model = Videojuego
		fields = [
		"nombre",
		"plataforma",
		"fecha_de_lanzamiento",
		"genero",
		"formato",
		]