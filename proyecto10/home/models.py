from django.db import models
from django.db.models.signals import post_save, pre_save, post_delete, pre_delete
from home import validators, signals

# Create your models here.
class Celular(models.Model):
	modelo = models.CharField(max_length=32)
	marca = models.CharField(max_length=32)
	memoria = models.IntegerField('Memoria (gbs)', validators = [validators.validation_memoria])
	color = models.CharField(max_length=32)
	caracteristica = models.CharField(max_length=75)


	def __str__(self):
		return self.modelo

post_save.connect(signals.post_save_home_receiver, sender = Celular)
pre_save.connect(signals.pre_save_home_receiver, sender = Celular)
post_delete.connect(signals.post_delete_home_receiver, sender = Celular)
pre_delete.connect(signals.pre_delete_home_receiver, sender = Celular)