from django.shortcuts import render, redirect
from django.views import generic
from django.urls import reverse_lazy
from django.db.models import Q

# Create your views here.
from .models import Pokemon
from .forms import PokemonForm

from .serializers import PokemonSerializer
from rest_framework import generics
from rest_framework import viewsets

class PokemonListAPIView(generics.ListAPIView):
	serializer_class = PokemonSerializer

	def get_queryset(self, *args, **kwargs):
		return Pokemon.objects.all()



class PokemonCreateAPIView(generics.CreateAPIView):
	serializer_class = PokemonSerializer

	def perform_create(self, serializer):
		serializer.save()




def list(request):
	pokemons = Pokemon.objects.all()
	if request.GET:
		pokemons = Pokemon.objects.filter(nombre = request.GET["q"])
	context = {
	"list_pokemons": pokemons
	}
	return render(request, "home/list.html", context)


def detail(request, id):
	queryset = Pokemon.objects.get(id = id)
	context = {
	"object": queryset
	}
	return render(request, "home/detail.html", context)

def create(request):
	form = PokemonForm(request.POST or None)
	if request.user.is_authenticated:
		error = "User Logged"
		if form.is_valid():
			form.save()
	else:
		error = "User Must Be Logged"

	context = {
	"form": form,
	"menssage": error
	}
	return render(request, "home/create.html", context)

def update(request, id):
	pokemon = Pokemon.objects.get(id=id)
	if  request.method == "GET":
		form = PokemonForm(instance=pokemon )
	else:
		form = PokemonForm(request.POST, instance=pokemon )
		if form.is_valid():
			form.save()
		return redirect("list")
	context = {
	"form": form
	}
	return render(request, "home/update.html", context)

def delete(request, id):
	pokemon = Pokemon.objects.get(id=id)
	if request.method == "POST":
		pokemon.delete()
		return redirect("list")
	context = {
		"object": pokemon
	}
	return render(request, "home/delete.html", context)






class Create(generic.CreateView):
	template_name = "home/create2.html"
	model = Pokemon
	fields = [
		"nombre",
		"tipo",
		"nivel",
	]
	success_url = reverse_lazy("list")

class List(generic.ListView):
	template_name = "home/list2.html"
	model = Pokemon

	def get_queryset(self, *args, **kwargs):
		return Pokemon.objects.all()

	def get_context_data(self, *args, **kwargs):
		context = super(List, self).get_context_data(*args, **kwargs)
		return context


class Detail(generic.DetailView):
	template_name = "home/detail2.html"
	model = Pokemon


class Update(generic.UpdateView):
	template_name = "home/update2.html"
	model = Pokemon
	fields = [
		"nombre",
		"tipo",
		"nivel",
	]
	success_url = reverse_lazy("list")

class Delete(generic.DeleteView):
	template_name = "home/delete2.html"
	model = Pokemon
	success_url = reverse_lazy("list")









def index1(request):
	return render(request, "home/index.html", {})

def index2(request):
	return render(request, "home/index2.html", {})

def index3(request):
	return render(request, "home/index3.html", {})

def index4(request):
	return render(request, "home/index4.html", {})

def index5(request):
	return render(request, "home/index5.html", {})

def index6(request):
	return render(request, "home/index6.html", {})

def index7(request):
	return render(request, "home/index7.html", {})

def index8(request):
	return render(request, "home/index8.html", {})

def index9(request):
	return render(request, "home/index9.html", {})

def index10(request):
	return render(request, "home/index10.html", {})