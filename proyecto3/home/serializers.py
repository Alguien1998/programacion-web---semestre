from rest_framework import serializers
from home.models import Registro

class RegistroSerializer(serializers.ModelSerializer):
	class Meta:
		model = Registro
		fields = [
			"id",
			"nombre",
			"apePat",
			"apeMat",
			"nacionalidad",
			"fechaNac",
			"lugarNac",
			"correo",
		]

